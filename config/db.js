const mongoose = require('mongoose')
const DB_URL= 'mongodb://localhost:27017/practica_desde_0'
const CONFIG_DB = { useNewUrlParser: true, useUnifiedTopology:true,}



const conectDB = async ()=>{
    try{
const response = await mongoose.connect(DB_URL,CONFIG_DB)
const {host, port, name,}= response.connection;
console.log(`conectado a ${name} en ${host},${port},`);
}
catch(error){
console.log('Error al Iniciar el servidor',error);
}
}


module.exports={
    conectDB,
    DB_URL,
    CONFIG_DB,
}