const { urlencoded } = require('express');
const express = require('express');
const { conectDB } = require('./config/db');
const moviesRoutes = require('./routes/movies.routes')
const cinemaRoutes = require('./routes/cinema.routes')
conectDB();
const PORT = 3000;
const server=  express();
const router= express.Router()

//esta linea le indica a express que cuando reciba datos desde postman con un JSON adjunto lo lea  y lo envie a req.body
server.use(express.json())
//si la peticion desde postman viene en formato urlencoded mete la info en req.body
server.use(express.urlencoded({extended: false}));

// controlador de errores (error handler)//esta parte tengo que repasarla ya que no la entiendo bien
server.use((error, req, res, next)=>{
    const status = error.status || 500
    const message = error.message || 'Unexpected error'
    return res.status(status).json(message);
})
//ruta inicial
router.get('/', (req,res) =>{
    return res.send('server funcionando')
})
server.use('/',router)
server.use('/peliculas',moviesRoutes)
server.use('/cines',cinemaRoutes)


const callback = () =>{ `arrancando servidor en http://localhost:${PORT}` }
server.listen(PORT,callback)


