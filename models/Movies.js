const mongoose = require('mongoose')
const Schema = mongoose.Schema

const moviesSchema = new Schema(
    {
        title: {type: 'string',required:true,},
        director:{type:'string',required:true,},
        year: {type:'number',required:false,},
        genre: {type:'string',required:false,}
    },
    {timestamps:true}
);


const MoviesModel = mongoose.model('Movies',moviesSchema);
module.exports=MoviesModel;
