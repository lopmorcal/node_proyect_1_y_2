const mongoose = require('mongoose')
const Schema = mongoose.Schema

const cinemaSchema = new Schema(
    {
        name: {type: 'string',required:true,},
        location:{type:'string',required:true,},
        movies: [{type:mongoose.Types.ObjectId,ref:'Movies', required:false,}],
        
    },
    {timestamps:true}
);


const cinemaModel = mongoose.model('Cinema',cinemaSchema);
module.exports=cinemaModel;