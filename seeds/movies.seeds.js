const mongoose = require('mongoose')
const MoviesModel = require('../models/Movies')
const {DB_URL,CONFIG_DB}=require('../config/db')


const moviesArray = [
    {
      title: 'The Matrix',
      director: 'Hermanas Wachowski',
      year: 1999,
      genre: 'Acción',
    },
    {
      title: 'The Matrix Reloaded',
      director: 'Hermanas Wachowski',
      year: 2003,
      genre: 'Acción',
    },
    {
      title: 'Buscando a Nemo',
      director: 'Andrew Stanton',
      year: 2003,
      genre: 'Animación',
    },
    {
      title: 'Buscando a Dory',
      director: 'Andrew Stanton',
      year: 2016,
      genre: 'Animación',
    },
    {
      title: 'Interestelar',
      director: 'Christopher Nolan',
      year: 2014,
      genre: 'Ciencia ficción',
    },
    {
      title: '50 primeras citas',
      director: 'Peter Segal',
      year: 2004,
      genre: 'Comedia romántica',
    },
  ];


mongoose.connect(DB_URL,CONFIG_DB)
.then(async()=>{console.log('Conectado a DB');
const allMovies= await MoviesModel.find()

if (allMovies.length){
  await MoviesModel.collection.drop()
  console.log('Coleccion eliminada');
}
})
.catch(err =>console.log('No se pudo encontrar en DB',err))

.then(async()=>{await MoviesModel.insertMany(moviesArray)
console.log('Nuevas peliculas añadidas');
})
.catch(err=> console.log('No se pudieron añadir peliculas',err))

.finally( () => mongoose.disconnect());



