const express = require('express')
const MoviesModel = require('../models/Movies');
const router= express.Router()

//metodo post
router.post('/create',async(req,res,next)=>{
    try{

        const {title, director, year, genre}= req.body;
        const newMovie= new MoviesModel({

            title,
            director,
            year,
            genre
        })
        const createMovie = await newMovie.save();

        console.log('Pelicula creada->',createMovie);
        return res.status(200).json(createMovie)

    }catch(error){
        return next(error)
    }
})
//metodo put
router.put('/modify/:id',async(req,res,next)=>{
    try{

        const {id}= req.params;
        const modifyMovie= new MoviesModel(req.body)
        modifyMovie._id = id
        const movieUpdated = await MoviesModel.findByIdAndUpdate(id,modifyMovie,{new:true})
        console.log('Pelicula modificada->',modifyMovie);
        return res.status(200).json(movieUpdated)

    }catch(error){
        return next(error)

    }
})
//metodo delete
router.delete('/delete/:id',async(req,res,next)=>{
    try{
    const{id}=req.params;
    const erased = await MoviesModel.findByIdAndDelete(id)
    if (erased) {
        res.status(200).json('Pelicula borrada con exito!')
    }else{
        res.status(404).json('La pelicula no exsiste o ya ha sido borrada')
    }
}catch (error){
   return next(error) 
}
})


//rutas o metodo get

router.get('/', async (req,res,next) =>{
    try{
        const movieList= await MoviesModel.find()
        return res.status(200).json(movieList)
    }catch(error){
        return next(error)
    }  
})
//id
router.get('/:id', async (req,res,next) =>{
    try{
        const id = req.params.id
        const idMovie= await MoviesModel.findById(id)
        
        if (idMovie) {
            return res.status(200).json(idMovie)
    
        }else{
            return next(error)
        }
    }catch(error){
        return next(error)
    }  
})
//titulo
router.get('/titulo/:title', async (req,res,next) =>{
    try{
        const title = req.params.title
        const titleMovies= await MoviesModel.find({title:title})
        
        if (titleMovies) {
            return res.status(200).json(titleMovies)
    
        }else{
            return next(error)
        }
    }catch(error){
        return next(error)
    }  
})
//genero
router.get('/genero/:genre', async (req,res,next) =>{
    try{
        const genre = req.params.genre
        const genreMovies= await MoviesModel.find({genre:genre})
        
        if (genreMovies) {
            return res.status(200).json(genreMovies)
    
        }else{
            return next(error)
        }
    }catch(error){
        return next(error)
    }  
})
//fecha
router.get('/fecha/:year', async (req,res,next) =>{
    try{
        const year = req.params.year
        const yearMovies= await MoviesModel.find({year:{$gt:year}})
        
        if (yearMovies) {
            return res.status(200).json(yearMovies)
    
        }else{
            return next(error)
        }
    }catch(error){
        return next(error)
    }  
})

module.exports=router;