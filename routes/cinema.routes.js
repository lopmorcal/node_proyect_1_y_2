const express = require('express')
const cinemaModel = require('../models/Cinema');
const router= express.Router()

//metodo post
router.post('/create',async(req,res,next)=>{
    try{

        const {name, location}= req.body;
        const newMovie= new cinemaModel({

           name,
           location,
           movies: []
        })
        const createCinema = await newMovie.save();

        console.log('Pelicula creada->',createCinema);
        return res.status(200).json(createCinema)

    }catch(error){
        return next(error)
    }
})
//metodo put
// router.put('/modify/:id',async(req,res,next)=>{
//     try{

//         const {id}= req.params;
//         const modifyCinema= new cinemaModel(req.body)
//         modifyCinema._id = id
//         const cinemaUpdated = await cinemaModel.findByIdAndUpdate(id,modifyCinema,{new:true})
//         console.log('Pelicula modificada->',modifyCinema);
//         return res.status(200).json(cinemaUpdated)

//     }catch(error){
//         return next(error)

//     }
// })
router.put('/modify',async(req,res,next)=>{

try{

    const{cinemaId,moviesId}=req.body;
    
    const uptdateCinema = await cinemaModel.findByIdAndUpdate(
        cinemaId,
        {$addToSet:{movies:moviesId}},
        {new:true},
    );
    return res.status(200).json(uptdateCinema)


}catch(error){
    return res.status(404).json(error)
}

});




//metodo delete
router.delete('/delete/:id',async(req,res,next)=>{
    try{
    const{id}=req.params;
    const erased = await cinemaModel.findByIdAndDelete(id)
    if (erased) {
        res.status(200).json('Borrada con exito!')
    }else{
        res.status(404).json('La Categoria no exsiste o ya ha sido borrada')
    }
}catch (error){
   return next(error) 
}
})


//rutas o metodo get

router.get('/', async (req,res,next) =>{
    try{
        const cinemaList= await cinemaModel.find().populate('movies')
        return res.status(200).json(cinemaList)
    }catch(error){
        return next(error)
    }  
})
//id
router.get('/:id', async (req,res,next) =>{
    try{
        const id = req.params.id
        const idCinema= await cinemaModel.findById(id)
        
        if (idCinema) {
            return res.status(200).json(idCinema)
    
        }else{
            return next(error)
        }
    }catch(error){
        return next(error)
    }  
})
//Location
router.get('/localizacion/:location', async (req,res,next) =>{
    try{
        const location = req.params.location
        const locationCinema= await cinemaModel.find({location:location})
        
        if (locationCinema) {
            return res.status(200).json(locationCinema)
    
        }else{
            return next(error)
        }
    }catch(error){
        return next(error)
    }  
})
//movies
router.get('/estrenos/:movies', async (req,res,next) =>{
    try{
        const movies = req.params.genre
        const moviesCinema= await cinemaModel.find({movies:movies})
        
        if (moviesCinema) {
            return res.status(200).json(moviesCinema)
    
        }else{
            return next(error)
        }
    }catch(error){
        return next(error)
    }  
})

module.exports=router;